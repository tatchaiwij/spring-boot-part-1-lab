package lab3;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Codecamp {
    @Value("${language}")
    private String language;

    @Value("${student}")
    private String student;

    public Codecamp() {

    }

    public String getLanguage() {
        return this.language;
    }

    public String getStudent() {
        return this.student;
    }
}