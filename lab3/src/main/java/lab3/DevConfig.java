package lab3;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@ComponentScan("lab3")
@PropertySource("classpath:application.properties")
public class DevConfig {

}
