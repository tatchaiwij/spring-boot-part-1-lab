package lab3;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleController {
    @RequestMapping("/")
    @ResponseBody

    String home() {
        try(AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DevConfig.class)){
            Codecamp codecamp = context.getBean(Codecamp.class);
            return codecamp.getLanguage() + codecamp.getStudent();
        }
    }
}